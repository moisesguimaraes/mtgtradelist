'''
Created on 07/01/2013

@author: moisesguimaraes
'''
from tests import TestCase
from app.models import Card

class Test(TestCase):
    
    def test_creation(self):
        fetched = Card.all().filter('number =', 104).ancestor(self.set_entity['isd']).fetch(1)[0]
        
        self.assertEquals(fetched.number, 104)
        self.assertEquals(fetched.name, 'Heartless Summoning')
        self.assertEquals(fetched.parent().name, self.set_data['isd'])
        self.assertEquals(fetched.parent().key().name(), 'isd')
        
        
    def test_find_by_parent(self):
        all_cards = Card.all().fetch(5)
        isd_cards = Card.all().ancestor(self.set_entity['isd']).fetch(5)
        dka_cards = Card.all().ancestor(self.set_entity['dka']).fetch(5)
        
        self.assertEquals(len(all_cards), 5)
        self.assertEquals(len(isd_cards), 3)
        self.assertEquals(len(dka_cards), 2)
        
        
    def test_get_image_url(self):
        fetch = Card.get_by_number(104, self.set_entity['isd'])
        
        self.assertEquals(fetch.get_image_url(), 'http://magiccards.info/scans/en/isd/104.jpg')
        
        fetch = Card.get_by_number(140, self.set_entity['dka'])
        self.assertEquals(fetch.get_image_url('a'), 'http://magiccards.info/scans/en/dka/140a.jpg')
