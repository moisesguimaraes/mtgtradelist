'''
Created on 08/01/2013

@author: moisesguimaraes
'''
from tests import TestCase
from app.models import Account

class Test(TestCase):
    
    def test_creation(self):
        account = Account.all().fetch(1)[0]
        
        self.assertEquals(len(account.want_list()), 2);
        self.assertEquals(len(account.have_list()), 3);
        
        self.assertEquals(len(self.isd_card_entity[104].have_list()), 2);
        self.assertEquals(len(self.isd_card_entity[78].want_list()), 1);
        
