'''
Created on 07/01/2013

@author: moisesguimaraes
'''
from tests import TestCase
from app.models import Set


class Test(TestCase):

    def test_creation(self):
        fetched = Set.get_by_key_name('isd')

        self.assertEquals(fetched.key().name(), 'isd')
        self.assertEquals(fetched.name, self.set_data['isd'])
        self.assertEquals(len(fetched.cards()), 3)
