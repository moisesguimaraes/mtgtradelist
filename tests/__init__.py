'''
Created on 13/01/2013

@author: moisesguimaraes
'''
import unittest
from app.models import Account, Set, Card, Want, Have
from google.appengine.api import users, apiproxy_stub_map, datastore_file_stub

class TestCase(unittest.TestCase):
    
    def setUp(self):
        # Use a fresh stub datastore.
        apiproxy_stub_map.apiproxy = apiproxy_stub_map.APIProxyStubMap()
        stub = datastore_file_stub.DatastoreFileStub('mtgtradelist', '/dev/null', '/dev/null')
        apiproxy_stub_map.apiproxy.RegisterStub('datastore_v3', stub)
        
        self.scenario_one()

    
    def scenario_one(self):
        self.email = 'moisesguimaraesm@gmail.com'

        self.set_data = {'isd' : 'Inistrad', 'dka' : 'Dark Ascension'}
        self.set_entity = {}
        
        self.isd_card_data = {104 : 'Heartless Summoning', 25 : 'Dissipate', 78 : 'Snapcaster Mage'}
        self.isd_card_entity = {}
        
        self.dka_card_data = {139 : 'Havengul Lich', 140 : 'Huntmaster of the Fells'}
        self.dka_card_entity = {}
        
        for key, name in self.set_data.items():
            self.set_entity[key] = Set(key_name=key, name=name)
            self.set_entity[key].put()

        for number, name in self.isd_card_data.items():
            self.isd_card_entity[number] = Card.new(number, name, self.set_entity['isd'])
            self.isd_card_entity[number].put()

        for number, name in self.dka_card_data.items():
            self.dka_card_entity[number] = Card.new(number, name, self.set_entity['dka'])
            self.dka_card_entity[number].put()

        account = Account.new(users.User(email=self.email)).put();

        Have(count=4, account=account, parent=self.isd_card_entity[104], state='NM', foil=True).put()
        Have(count=4, account=account, parent=self.isd_card_entity[104], state='NM').put()
        Have(count=3, account=account, parent=self.dka_card_entity[139], state='NM').put()
        
        Want(count=2, account=account, parent=self.isd_card_entity[78]).put()
        Want(count=1, account=account, parent=self.isd_card_entity[25]).put()

                    
