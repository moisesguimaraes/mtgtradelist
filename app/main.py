import webapp2, jinja2
from google.appengine.api import users
from app.models import Account

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader('templates'))

class Index(webapp2.RequestHandler):
    
    def get(self):
        template = jinja_environment.get_template('index.html')
        self.response.out.write(template.render({}))



class SingUp(webapp2.RequestHandler):

    def get(self):
        self.response.out.write('Sing Up!')
        

    def post(self):
        user = users.get_current_user()
        name = self.request.name
        
        account = Account.new(user)
        account.name = name
        account.put()
        
        self.redirect('/')


app = webapp2.WSGIApplication([('/', Index), ('/singup', SingUp)], debug=True)
