'''
Created on 07/01/2013

@author: moisesguimaraes
'''
from google.appengine.api import urlfetch
from google.appengine.ext import db, blobstore


class Account(db.Model):
    google_user = db.UserProperty(required=True)
    name = db.StringProperty()
    
    @classmethod
    def new(cls, user):
        return Account(key_name=user.user_id(), google_user=user)
    
    
    def want_list(self):
        return self.want_set.fetch(500)
    
    
    def have_list(self):
        return self.have_set.fetch(500)



class Set(db.Model):
    #key = trigram
    name = db.StringProperty(required=True)
    blob = blobstore.BlobReferenceProperty()
    
    def cards(self):
        return Card.all().ancestor(self).fetch(300)



class Card(db.Model):
    number = db.IntegerProperty(required=True)
    name = db.StringProperty(required=True)
    image = db.BlobProperty()

    @classmethod
    def new(cls, number, name, edition):
        return Card(number=number, name=name, parent=edition)
    
    
    @classmethod
    def get_by_number(cls, number, edition):
        return Card.all().filter('number =', number).ancestor(edition).get()
    
    
    def get_image_url(self, sufix=''):
        '''
        Retorna url da imagem em magiccards.info
        '''
        return 'http://magiccards.info/scans/en/' + self.parent().key().name() + '/' + str(self.number) + sufix + '.jpg'
    
    
    def fetch_image(self): # pragma: no cover
        if not self.image:
            result = urlfetch.fetch(self.get_image_url())
        
            if result.status_code == 404:
                result.urlfetch.fetch(self.get_image_url('a'))
                
            if result.status_code == 200:
                self.image = result.content
                
        return self.image != None
            
        
        
    def want_list(self):
        return Want.all().ancestor(self).fetch(10)
    
    
    def have_list(self):
        return Have.all().ancestor(self).fetch(10)
    
    
    
class Want(db.Model):
    count = db.IntegerProperty(required=True)
    account = db.ReferenceProperty(Account, required=True)



card_conditions = {
   'NM' : 'Near Mint/Mint',
   'SP' : 'Slightly Played',
   'MP' : 'Moderately Played',
   'HP' : 'Heavily Played',
}

class Have(db.Model):
    count = db.IntegerProperty(required=True)
    account = db.ReferenceProperty(Account, required=True)
    state = db.StringProperty(required=True, choices=card_conditions.keys())
    foil = db.BooleanProperty(default=False)
